FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache radicale daemonize
COPY ./monit.conf /etc/monit.d/radicale.conf
COPY --chown=root:radicale ./config /etc/radicale/config

EXPOSE 5232
